"use strict"

// Завдання 1
// Створіть на  основі двох масивiв один масив, який буде об'єднанням двох масивів без повторюваних прізвищ клієнтів.

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const result = [...new Set([...clients1, ...clients2])];
console.log("Task 1(result)", result);

// Завдання 2
// Створіть на основі масиву масив charactersShortInfo, що складається з об'єктів, у яких є тільки 3 поля - ім'я, прізвище та вік.
const characters = [
    {
      name: "Елена",
      lastName: "Гилберт",
      age: 17, 
      gender: "woman",
      status: "human"
    },
    {
      name: "Кэролайн",
      lastName: "Форбс",
      age: 17,
      gender: "woman",
      status: "human"
    },
    {
      name: "Аларик",
      lastName: "Зальцман",
      age: 31,
      gender: "man",
      status: "human"
    },
    {
      name: "Дэймон",
      lastName: "Сальваторе",
      age: 156,
      gender: "man",
      status: "vampire"
    },
    {
      name: "Ребекка",
      lastName: "Майклсон",
      age: 1089,
      gender: "woman",
      status: "vempire"
    },
    {
      name: "Клаус",
      lastName: "Майклсон",
      age: 1093,
      gender: "man",
      status: "vampire"
    }
  ];

  let charactersShortInfo= [...characters].map(x =>{
    return {
        name: x.name,
        lastName: x.lastName,
        age:x.age
    }
  })

console.log("Task 2(input data)", characters)
console.log("Task 2 (result)", charactersShortInfo)


// Завдання 3
// Напишіть деструктуруюче присвоєння, яке:
// властивість name присвоїть в змінну ім'я
// властивість years присвоїть в змінну вік
// властивість isAdmin присвоює в змінну isAdmin false, якщо такої властивості немає в об'єкті
// Виведіть змінні на екран.

const user1 = {
    name: "John",
    years: 30,
  };

  let {name,years, isAdmin = false} = user1;

  console.log("Task 3 (result)", name, years, isAdmin)

// Завдання 4
// Напишіть код, який складе повне досьє про можливу особу Сатоші Накамото. Змінювати об'єкти satoshi2018, satoshi2019, satoshi2020 не можна.
// Об'єднати дані з цих трьох об'єктів в один об'єкт - fullProfile.
// Поля в об'єктах можуть повторюватися. В об'єкті має зберегтися значення, яке було отримано пізніше (наприклад, значення з 2020 пріоритетніше порівняно з 2019).

const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
      lat: 38.869422, 
      lng: 139.876632
    }
  }
  
  const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
  }
  
  const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto', 
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
  }

const fullProfile = {
    ...satoshi2018,
    ...satoshi2019,
    ...satoshi2020,
  };
  console.log("Task 4 (result)", fullProfile);
  



//   Завдання 5
//   Дано масив книг. Вам потрібно додати до нього ще одну книгу, не змінюючи існуючий масив (в результаті операції має бути створено новий масив).

const books = [{
  name: 'Harry Potter',
  author: 'J.K. Rowling'
}, {
  name: 'Lord of the rings',
  author: 'J.R.R. Tolkien'
}, {
  name: 'The witcher',
  author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
  name: 'Game of thrones',
  author: 'George R. R. Martin'
}

let newArrayOfBooks = [...books];
newArrayOfBooks.push({...bookToAdd});
console.log("Task 5(input data)", books)
console.log("Task 5(result)", newArrayOfBooks)

// Завдання 6
// Даний об'єкт employee. Додайте до нього властивості age і salary, не змінюючи початковий об'єкт (має бути створено новий об'єкт, який включатиме всі необхідні властивості). Виведіть новий об'єкт у консоль.

const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
  }
const newEmployee = {
    ...employee,
    age: 34,
    salary: "4050$",
  }
console.log("Task 6(input data)", employee);
console.log("Task 6(result)", newEmployee);

// Завдання 7
// Доповніть код так, щоб він коректно працював

const array = ['value', () => 'showValue'];

const [value, showValue] = array; //дописала код тут

alert(value); // має бути виведено 'value'
alert(showValue());  // має бути виведено 'showValue'